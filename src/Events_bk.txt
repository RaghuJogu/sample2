<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>JNVW Alumni Association</title>
<link href="/css/bootstrap.min.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-min.js"></script>

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->


<script type="text/javascript" src="/js/bootstrap-custom.js"></script>
<script src="https://www.w3schools.com/lib/w3data.js"></script>
<link href="/css/styles.css" rel="stylesheet"/>
 <link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>


<style type="text/css">
	.form-group.required .control-label:after {
  content:"*";
  color:red;
}

 
.thumbnail {
    position:relative;
    overflow:hidden;
}
 
.caption {
    position:absolute;
    top:-100%;
    right:0;
    background:rgba(66, 139, 202, 0.75);
    width:100%;
    height:100%;
    padding:2%;
    text-align:center;
    color:#fff !important;
    z-index:2;
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    -o-transition: all 0.5s ease-in-out;
    -ms-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
}
.thumbnail:hover .caption {
    top:0%;
}
    

</style>
<script type="text/javascript">

$(window).bind("load", function() {
	setActiveClassForNav("a_news"); 
});
</script>
</head>
<body>
<div class="menu">
<div w3-include-html="header.php"></div>
<div class="container mainContent">
  
    <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <!-- <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger">Zoom</a>
                    <a href="" class="label label-default">Download</a></p> -->
                </div>
                <img src="/images/e1.jpg" alt="...">
            </div>
        </div>
      
        <div class="col-xs-6 col-sm-4 col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <!-- <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger">Zoom</a>
                    <a href="" class="label label-default">Download</a></p> -->
                </div>
                <img src="/images/e2.jpg" alt="...">
            </div>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <!-- <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger">Zoom</a>
                    <a href="" class="label label-default">Download</a></p> -->
                </div>
                <img src="/images/e3.jpg" alt="...">
            </div>
        </div>

		<div class="col-xs-6 col-sm-4 col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <!-- <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger">Zoom</a>
                    <a href="" class="label label-default">Download</a></p> -->
                </div>
                <img src="/images/e1.jpg" alt="...">
            </div>
        </div> 
    </div>
    
  <!-- /.container -->
</div>
<div w3-include-html="footer.html"></div>
</div>

<script>
w3IncludeHTML();
</script>
</body>
</html>
